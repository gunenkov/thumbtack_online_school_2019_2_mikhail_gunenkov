package net.thumbtack.school.figures.v1;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class TestPoint {
    @Test
    public void testPointMovement() {
        Point point1 = new Point(10, 20);
        Point point2 = new Point();
        point2.moveTo(20, 20);
        point2.moveRel(-10, 0);
        assertEquals(point1, point2);
    }
}
