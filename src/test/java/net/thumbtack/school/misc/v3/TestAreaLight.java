package net.thumbtack.school.misc.v3;

import net.thumbtack.school.colors.v3.Color;
import net.thumbtack.school.colors.v3.ColorException;
import net.thumbtack.school.figures.v3.Point;
import net.thumbtack.school.iface.v3.Colored;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class TestAreaLight {
    @Test
    public void testAreaLight1() {
        Colored areaLight1 = new AreaLight(5.0);
        SpotLight areaLight2 = new AreaLight();
        AreaLight areaLight3 = new AreaLight();
        assertAll(
                () -> assertEquals(Color.RED, areaLight1.getColor()),
                () -> assertEquals(0, areaLight2.getPosition().getY()),
                () -> assertEquals(areaLight2, areaLight3)
        );
    }

    @Test
    public void testAreaLight2() {
        AreaLight areaLight = new AreaLight();
        areaLight.setPosition(new Point(5, 8));
        areaLight.setColor(Color.BLUE);
        areaLight.moveTo(10, 20);
        areaLight.setArea(120.0);
        assertAll(
                () -> assertEquals(Color.BLUE, areaLight.getColor()),
                () -> assertEquals(10, areaLight.getPosition().getX()),
                () -> assertEquals(120.0, areaLight.getArea())
        );
        areaLight.moveTo(new Point(60, 70));
        areaLight.moveRel(-60, -70);
        areaLight.setArea(0);
        areaLight.setColor(Color.RED);
        assertEquals(new AreaLight(), areaLight);
    }

    @Test
    public void testAreaLight3() throws ColorException {
        AreaLight areaLight1 = new AreaLight(6, 16, "GREEN", 10);
        AreaLight areaLight2 = new AreaLight(new Point(6, 16), "GREEN", 10);
        AreaLight areaLight3 = new AreaLight("RED", 50);
        assertAll(
                () -> assertEquals(areaLight1, areaLight2),
                () -> assertEquals(Color.colorFromString("GREEN"), areaLight1.getColor()),
                () -> assertEquals(50, areaLight3.getArea())
        );
    }
}
