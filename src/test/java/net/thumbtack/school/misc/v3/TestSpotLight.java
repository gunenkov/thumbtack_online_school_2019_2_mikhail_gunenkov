package net.thumbtack.school.misc.v3;

import net.thumbtack.school.colors.v3.Color;
import net.thumbtack.school.colors.v3.ColorException;
import net.thumbtack.school.figures.v3.Point;
import net.thumbtack.school.iface.v3.Colored;
import net.thumbtack.school.iface.v3.Movable;
import org.junit.jupiter.api.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

public class TestSpotLight {
    @Test
    public void testSpotLight1() {
        Movable spotLight1 = new SpotLight();
        Colored spotLight2 = new SpotLight(0,0, Color.RED);
        SpotLight spotLight3 = new SpotLight(5,10,Color.GREEN);
        assertAll(
                () -> assertEquals(spotLight1, spotLight2),
                () -> assertEquals(5, spotLight3.getPosition().getX()),
                () -> assertEquals(10, spotLight3.getPosition().getY())
        );
    }

    @Test
    public void testSpotLight2() {
        Movable spotLight1 = new SpotLight();
        Colored spotLight2 = new SpotLight(0,0,Color.RED);
        spotLight1.moveTo(5,10);
        spotLight1.moveRel(10, 20);
        assertEquals(new Point(15, 30), ((SpotLight) spotLight1).getPosition());
        ((SpotLight) spotLight1).setColor(Color.BLUE);
        assertEquals(Color.BLUE, ((SpotLight) spotLight1).getColor());
        spotLight1.moveTo(((SpotLight) spotLight2).getPosition());
        assertNotEquals(spotLight1, spotLight2);
        ((SpotLight) spotLight1).setColor(Color.RED);
        assertEquals(spotLight1, spotLight2);
    }

    @Test
    public void testSpotLight3() throws ColorException {
        SpotLight spotLight = new SpotLight("GREEN");
        assertEquals(Color.GREEN, spotLight.getColor());
        spotLight.setColor(Color.colorFromString("BLUE"));
        assertEquals(Color.BLUE, spotLight.getColor());
        spotLight.setColor(Color.colorFromString("RED"));
        assertEquals(Color.RED, spotLight.getColor());

        try {
            spotLight.setColor((String) null);
            fail();
        } catch (ColorException ex) {
            assertEquals("Переданное текстовое представление имеет значение Null", ex.getErrorCode().getErrorString());
        }
    }

    @Test
    public void testSpotLight4() throws ColorException {
        SpotLight spotLight1 = new SpotLight(new Point(6,16), "BLUE");
        SpotLight spotLight2 = new SpotLight(6,16,"BLUE");
        assertEquals(spotLight1, spotLight2);
    }
}
