package net.thumbtack.school.misc.v2;

import net.thumbtack.school.figures.v2.Point;
import net.thumbtack.school.iface.v2.Colored;
import net.thumbtack.school.iface.v2.Movable;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class TestSpotLight {
    @Test
    public void testSpotLight1() {
        Movable spotLight1 = new SpotLight();
        Colored spotLight2 = new SpotLight(0, 0, 1);
        SpotLight spotLight3 = new SpotLight(5, 10, 2);
        assertAll(
                () -> assertEquals(spotLight1, spotLight2),
                () -> assertEquals(5, spotLight3.getPosition().getX()),
                () -> assertEquals(10, spotLight3.getPosition().getY())
        );
    }

    @Test
    public void testSpotLight2() {
        Movable spotLight1 = new SpotLight();
        Colored spotLight2 = new SpotLight(0, 0, 1);
        spotLight1.moveTo(5, 10);
        spotLight1.moveRel(10, 20);
        assertEquals(new Point(15, 30), ((SpotLight) spotLight1).getPosition());
        ((SpotLight) spotLight1).setColor(3);
        assertEquals(3, ((SpotLight) spotLight1).getColor());
        spotLight1.moveTo(((SpotLight) spotLight2).getPosition());
        assertNotEquals(spotLight1, spotLight2);
        ((SpotLight) spotLight1).setColor(1);
        assertEquals(spotLight1, spotLight2);
    }
}
