package net.thumbtack.school.misc.v2;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class TestRegion {
    @Test
    public void testRegion() {
        Region region1 = new Region("Omsk", 500000);
        Region region2 = new Region("Moscow");
        Region region3 = new Region("Omsk", 500000);

        assertAll(
                () -> assertEquals("Omsk", region1.getName()),
                () -> assertEquals(500000, region1.getArea()),
                () -> assertEquals(0, region2.getArea()),
                () -> assertEquals(region1, region3)
        );
    }

    @Test
    public void testRegion2() {
        Region region = new Region("Novosibirsk");
        region.setArea(4000);
        assertEquals(4000, region.getArea());
        region.setName("Omsk");
        assertEquals("Omsk", region.getName());
    }
}
