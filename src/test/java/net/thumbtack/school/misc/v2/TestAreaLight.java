package net.thumbtack.school.misc.v2;

import net.thumbtack.school.figures.v2.Point;
import net.thumbtack.school.iface.v2.Colored;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class TestAreaLight {
    @Test
    public void testAreaLight1() {
        Colored areaLight1 = new AreaLight(5.0);
        SpotLight areaLight2 = new AreaLight();
        AreaLight areaLight3 = new AreaLight();
        assertAll(
                () -> assertEquals(1, areaLight1.getColor()),
                () -> assertEquals(0, areaLight2.getPosition().getY()),
                () -> assertEquals(areaLight2, areaLight3)
        );
    }

    @Test
    public void testAreaLight2() {
        AreaLight areaLight = new AreaLight();
        areaLight.setPosition(new Point(5, 8));
        areaLight.setColor(6);
        areaLight.moveTo(10, 20);
        areaLight.setArea(120.0);
        assertAll(
                () -> assertEquals(6, areaLight.getColor()),
                () -> assertEquals(10, areaLight.getPosition().getX()),
                () -> assertEquals(120.0, areaLight.getArea())
        );
        areaLight.moveTo(new Point(60, 70));
        areaLight.moveRel(-60, -70);
        areaLight.setArea(0);
        areaLight.setColor(1);
        assertEquals(new AreaLight(), areaLight);
    }
}
