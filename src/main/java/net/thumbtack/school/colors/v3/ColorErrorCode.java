package net.thumbtack.school.colors.v3;

public enum ColorErrorCode {
    WRONG_COLOR_STRING("Переданная строка не является тектовым представлением одной из цветовых констант"),
    NULL_COLOR("Переданное текстовое представление имеет значение Null");

    String errorString;

    ColorErrorCode(String errorString) {
        this.errorString = errorString;
    }

    public String getErrorString() {
        return errorString;
    }
}
