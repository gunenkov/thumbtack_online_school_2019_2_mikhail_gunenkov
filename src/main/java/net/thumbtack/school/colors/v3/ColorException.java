package net.thumbtack.school.colors.v3;

public class ColorException extends Exception {
    private ColorErrorCode errorCode;

    public ColorException(ColorErrorCode colorErrorCode) {
        this.errorCode = colorErrorCode;
    }

    public ColorErrorCode getErrorCode() {
        return errorCode;
    }
}
