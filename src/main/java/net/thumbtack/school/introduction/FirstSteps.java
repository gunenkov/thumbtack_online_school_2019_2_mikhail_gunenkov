package net.thumbtack.school.introduction;

import java.util.OptionalInt;
import java.util.stream.IntStream;

public class FirstSteps {

    public int sum(int x, int y) {
        return x + y;
    }

    public int mul(int x, int y) {
        return x * y;
    }

    // гарантируется, что y != 0, поэтому без проверки
    public int div(int x, int y) {
        return x / y;
    }

    // гарантируется, что y != 0, поэтому без проверки
    public int mod(int x, int y) {
        return x % y;
    }

    public boolean isEqual(int x, int y) {
        return x == y;
    }

    public boolean isGreater(int x, int y) {
        return x > y;
    }

    public boolean isInsideRect(int xLeft, int yTop, int xRight, int yBottom, int x, int y) {
        // FIXED
        // REVU лишние скобки
        return (xLeft <= x && x <= xRight) && (yTop <= y && y <= yBottom);
    }

    public int sum(int[] array) {
        int sum = 0;
        for (int number : array) {
            sum += number;
        }
        return sum;
    }

    public int mul(int[] array) {
        int mul = array.length > 0 ? 1 : 0;
        for (int number : array) {
            mul *= number;
        }
        return mul;
    }

    public int min(int[] array) {
        int min = Integer.MAX_VALUE;
        for (int number : array) {
            if (number < min) min = number;
        }
        return min;
    }

    public int max(int[] array) {
        int max = Integer.MIN_VALUE;
        for (int number : array) {
            if (number > max) max = number;
        }
        return max;
    }

    public double average(int[] array) {
        if (array.length == 0) return 0.0;
        else {
            // FIXED
            // REVU есть sum, вызовите ее
            return IntStream.of(array).average().getAsDouble();
        }
    }

    public boolean isSortedDescendant(int[] array) {
        for (int i = 1; i < array.length; i++) {
            if (array[i] >= array[i - 1]) return false;
        }
        return true;
    }

    public void cube(int[] array) {
        for (int i = 0; i < array.length; i++) {
            // FIXED
            // REVU два умножения быстрее
            array[i] = array[i] * array[i] * array[i];
        }
    }

    public boolean find(int[] array, int value) {
        for (int number : array) {
            if (number == value) return true;
        }
        return false;
    }

    public void reverse(int[] array) {
        for (int i = 0; i < array.length / 2; i++) {
            array[i] = array[i] + array[array.length - 1 - i] - (array[array.length - 1 - i] = array[i]);
        }
    }

    public boolean isPalindrome(int[] array) {
        for (int i = 0; i < array.length / 2; i++) {
            if (array[i] != array[array.length - 1 - i]) return false;
        }
        return true;
    }

    public int sum(int[][] matrix) {
        int sum = 0;
        // FIXED
        // REVU есть sum для линейного массива, вызовите ее 
        for (int[] row : matrix) {
            sum += IntStream.of(row).sum();
        }
        return sum;
    }

    public int max(int[][] matrix) {
        int max = Integer.MIN_VALUE;
        OptionalInt suspectedMax;
        //FIXED
        // REVU есть max для линейного массива, вызовите ее 
        for (int[] row : matrix) {
            suspectedMax = IntStream.of(row).max();
            if (!suspectedMax.isPresent()) return max;
            if (suspectedMax.getAsInt() > max) max = suspectedMax.getAsInt();
        }
        return max;
    }

    public int diagonalMax(int[][] matrix) {
        int dMax = Integer.MIN_VALUE;
        int i = 0;
        for (int[] row : matrix) {
            if (row[i] > dMax) dMax = row[i];
            i++;
        }
        return dMax;
    }

    public boolean isSortedDescendant(int[][] matrix) {
        for (int[] row : matrix) {
            if (!isSortedDescendant(row)) return false;
        }
        return true;
    }

}
