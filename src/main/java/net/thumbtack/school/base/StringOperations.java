package net.thumbtack.school.base;

import java.text.DecimalFormat;

public class StringOperations {
    public static int getSummaryLength(String[] strings) {
        int summaryLength = 0;
        for (String s : strings) {
            summaryLength += s.length();
        }
        return summaryLength;
    }

    public static String getFirstAndLastLetterString(String string) {
        return new StringBuilder().append(string.charAt(0))
                .append(string.charAt(string.length() - 1)).toString();
    }

    public static boolean isSameCharAtPosition(String string1, String string2, int index) {
        return string1.charAt(index) == string2.charAt(index);
    }

    public static boolean isSameFirstCharPosition(String string1, String string2, char character) {
        return string1.indexOf(character) == string2.indexOf(character);
    }

    public static boolean isSameLastCharPosition(String string1, String string2, char character) {
        return string1.lastIndexOf(character) == string2.lastIndexOf(character);
    }

    public static boolean isSameFirstStringPosition(String string1, String string2, String str) {
        return string1.indexOf(str) == string2.indexOf(str);
    }

    public static boolean isSameLastStringPosition(String string1, String string2, String str) {
        return string1.lastIndexOf(str) == string2.lastIndexOf(str);
    }

    public static boolean isEqual(String string1, String string2) {
        return string1.equals(string2);
    }

    public static boolean isEqualIgnoreCase(String string1, String string2) {
        return string1.equalsIgnoreCase(string2);
    }

    public static boolean isLess(String string1, String string2) {
        return string1.compareTo(string2) < 0;
    }

    public static boolean isLessIgnoreCase(String string1, String string2) {
        return string1.compareToIgnoreCase(string2) < 0;
    }

    public static String concat(String string1, String string2) {
        return string1.concat(string2);
    }

    public static boolean isSamePrefix(String string1, String string2, String prefix) {
        return string1.startsWith(prefix) && string2.startsWith(prefix);
    }

    public static boolean isSameSuffix(String string1, String string2, String suffix) {
        return string1.endsWith(suffix) && string2.endsWith(suffix);
    }

    public static String getCommonPrefix(String string1, String string2) {
        // FIXED
        // REVU не надо сцеплять символы и StringBuilder не нужен
        // просто считайте совпадающие символы, а потом substring на число их
        int countOfCommonSymbols = 0;
        for (int i = 0; i < string1.length() && i < string2.length(); i++) {
            if (string1.charAt(i) == string2.charAt(i)) {
                countOfCommonSymbols++;
            } else break;
        }
        return string1.substring(0, countOfCommonSymbols);
    }

    public static String reverse(String string) {
        return new StringBuilder(string).reverse().toString();
    }

    public static boolean isPalindrome(String string) {
        // FIXED
        // REVU а можно без создания новой строки ?
        for (int i = 0; i < string.length() / 2; i++) {
            if (string.charAt(i) != string.charAt(string.length() - i - 1))
                return false;
        }
        return true;
    }

    public static boolean isPalindromeIgnoreCase(String string) {
        return isPalindrome(string.toLowerCase());
    }

    public static String getLongestPalindromeIgnoreCase(String[] strings) {
        // FIXED
        // REVU просто java.lang не нужно
        String longestPalindrome = "";
        for (String s : strings) {
            if (isPalindromeIgnoreCase(s) &&
                    s.length() > longestPalindrome.length()) {
                longestPalindrome = s;
            }
        }
        return longestPalindrome;
    }

    public static boolean hasSameSubstring(String string1, String string2, int index, int length) {
        if (index + length <= string1.length() && index + length <= string2.length()) {
            return string1.substring(index, length).equals(string2.substring(index, length));
        } else return false;
    }

    public static boolean isEqualAfterReplaceCharacters(String string1, char replaceInStr1, char replaceByInStr1, String string2, char replaceInStr2, char replaceByInStr2) {
        return string1.replace(replaceInStr1, replaceByInStr1)
                .equals(string2.replace(replaceInStr2, replaceByInStr2));
    }

    public static boolean isEqualAfterReplaceStrings(String string1, String replaceInStr1, String replaceByInStr1, String string2, String replaceInStr2, String replaceByInStr2) {
        return string1.replace(replaceInStr1, replaceByInStr1)
                .equals(string2.replace(replaceInStr2, replaceByInStr2));
    }

    public static boolean isPalindromeAfterRemovingSpacesIgnoreCase(String string) {
        return isPalindromeIgnoreCase(string.replace(" ", ""));
    }

    public static boolean isEqualAfterTrimming(String string1, String string2) {
        return isEqual(string1.trim(), string2.trim());
    }

    public static String makeCsvStringFromInts(int[] array) {
        return makeCsvStringBuilderFromInts(array).toString();
    }

    public static String makeCsvStringFromDoubles(double[] array) {
        return makeCsvStringBuilderFromDoubles(array).toString();
    }

    public static StringBuilder makeCsvStringBuilderFromInts(int[] array) {
        StringBuilder csvString = new StringBuilder();
        if (array.length == 0) return csvString;
        for (int number : array) {
            csvString.append(number).append(',');
        }
        return csvString.deleteCharAt(csvString.length() - 1);
    }

    public static StringBuilder makeCsvStringBuilderFromDoubles(double[] array) {
        DecimalFormat decimalFormat = new DecimalFormat("#.00");
        StringBuilder csvString = new StringBuilder();
        if (array.length == 0) return csvString;
        for (double element : array) {
            csvString.append(decimalFormat.format(element)).append(',');
        }
        return csvString.deleteCharAt(csvString.length() - 1);
    }

    public static StringBuilder removeCharacters(String string, int[] positions) {
        StringBuilder newString = new StringBuilder(string);
        for (int i = 0, offset = 0; i < positions.length; i++, offset++) {
            newString.deleteCharAt(positions[i] - offset);
        }
        return newString;
    }

    public static StringBuilder insertCharacters(String string, int[] positions, char[] characters) {
        StringBuilder newString = new StringBuilder(string);
        for (int i = 0, offset = 0; i < positions.length; i++, offset++) {
            newString.insert(positions[i] + offset, characters[i]);
        }
        return newString;
    }
}
