package net.thumbtack.school.boxes.v3;

import net.thumbtack.school.figures.v3.Figure;
import net.thumbtack.school.iface.v3.HasArea;

public class PairBox<T extends Figure, W extends Figure> implements HasArea {
    private T contentFirst;
    private W contentSecond;

    public PairBox(T contentFirst, W contentSecond) {
        this.contentFirst = contentFirst;
        this.contentSecond = contentSecond;
    }

    public static boolean isAreaEqual(PairBox<?, ?> firstPairBox, PairBox<?, ?> secondPairBox) {
        return firstPairBox.getArea() == secondPairBox.getArea();
    }

    public T getContentFirst() {
        return contentFirst;
    }

    public void setContentFirst(T contentFirst) {
        this.contentFirst = contentFirst;
    }

    public W getContentSecond() {
        return contentSecond;
    }

    public void setContentSecond(W contentSecond) {
        this.contentSecond = contentSecond;
    }

    @Override
    public double getArea() {
        return contentFirst.getArea() + contentSecond.getArea();
    }

    public boolean isAreaEqual(PairBox<?, ?> anotherPairBox) {
        return getArea() == anotherPairBox.getArea();
    }
}
