package net.thumbtack.school.boxes.v3;

import net.thumbtack.school.figures.v3.Figure;
import net.thumbtack.school.iface.v3.HasArea;

public class Box<T extends Figure> implements HasArea {
    private T content;

    public Box(T content) {
        this.content = content;
    }

    public static boolean isAreaEqual(Box<?> firstBox, Box<?> secondBox) {
        return firstBox.getArea() == secondBox.getArea();
    }

    public T getContent() {
        return content;
    }

    public void setContent(T content) {
        this.content = content;
    }

    @Override
    public double getArea() {
        return content.getArea();
    }

    public boolean isAreaEqual(Box<?> anotherBox) {
        return getArea() == anotherBox.getArea();
    }
}
