package net.thumbtack.school.figures.v3;

import java.util.Objects;

public class Ellipse extends Figure {
    private Point center;
    private Integer xAxis;
    private Integer yAxis;

    public Ellipse(Point center, int xAxis, int yAxis) {
        this.center = center;
        this.xAxis = xAxis;
        this.yAxis = yAxis;
    }

    public Ellipse(int xCenter, int yCenter, int xAxis, int yAxis) {
        // FIXED
        // REVU вызовите другой конструктор - this(...)
        this(new Point(xCenter, yCenter), xAxis, yAxis);
    }

    public Ellipse(int xAxis, int yAxis) {
        this(new Point(0, 0), xAxis, yAxis);
    }

    public Ellipse() {
        this(new Point(), 1, 1);
    }

    public Point getCenter() {
        return center;
    }

    public int getXAxis() {
        return xAxis;
    }

    public int getYAxis() {
        return yAxis;
    }

    public void setXAxis(Integer xAxis) {
        this.xAxis = xAxis;
    }

    public void setYAxis(Integer yAxis) {
        this.yAxis = yAxis;
    }

    public void setCenter(Point center) {
        this.center = center;
    }

    @Override
    public void moveTo(int x, int y) {
        center.setX(x);
        center.setY(y);
    }

    @Override
    public void moveRel(int dx, int dy) {
        center.setX(getCenter().getX() + dx);
        center.setY(getCenter().getY() + dy);
    }

    @Override
    public void resize(double ratio) {
        stretch(ratio, ratio);
    }

    public void stretch(double xRatio, double yRatio) {
        xAxis = (int) (xRatio * xAxis);
        yAxis = (int) (yRatio * yAxis);
    }

    @Override
    public double getArea() {
        return Math.PI * 0.25 * xAxis * yAxis;
    }

    @Override
    public double getPerimeter() {
        return 2 * Math.PI * Math.sqrt(0.5 * (Math.pow(xAxis / 2, 2.0) +
                Math.pow(yAxis / 2, 2.0)));
    }

    @Override
    public boolean isInside(int x, int y) {
        int absX = Math.abs(x - center.getX());
        int absY = Math.abs(y - center.getY());

        return (absX * absX) / (xAxis * xAxis * 0.25) + (absY * absY) / (yAxis * yAxis * 0.25) <= 1.0;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Ellipse ellipse = (Ellipse) o;
        return Objects.equals(center, ellipse.center) &&
                Objects.equals(xAxis, ellipse.xAxis) &&
                Objects.equals(yAxis, ellipse.yAxis);
    }

    @Override
    public int hashCode() {
        return Objects.hash(center, xAxis, yAxis);
    }
}

