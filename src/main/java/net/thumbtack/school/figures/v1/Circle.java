package net.thumbtack.school.figures.v1;

import java.util.Objects;

public class Circle {
    private Point center;
    private Integer radius;

    public Circle(Point center, int radius) {
        this.center = center;
        this.radius = radius;
    }

    public Circle(int xCenter, int yCenter, int radius) {
        center = new Point(xCenter, yCenter);
        this.radius = radius;
    }

    public Circle(int radius) {
        center = new Point();
        this.radius = radius;
    }

    public Circle() {
        this(1);
    }

    public Point getCenter() {
        return center;
    }

    public int getRadius() {
        return radius;
    }

    public void setCenter(Point center) {
        this.center = center;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    public void moveTo(int x, int y) {
        center.setX(x);
        center.setY(y);
    }

    public void moveTo(Point point) {
        center = point;
    }

    public void moveRel(int dx, int dy) {
        center.setX(getCenter().getX() + dx);
        center.setY(getCenter().getY() + dy);
    }

    public void resize(double ratio) {
        radius = (int) (ratio * radius);
    }

    public double getArea() {
        return Math.PI * radius * radius;
    }

    public double getPerimeter() {
        return 2 * Math.PI * radius;
    }

    public boolean isInside(int x, int y) {
        int absX = Math.abs(x - center.getX());
        int absY = Math.abs(y - center.getY());

        return Math.pow(absX, 2.0) + Math.pow(absY, 2.0) <= Math.pow(radius, 2.0);
    }

    public boolean isInside(Point point) {
        return isInside(point.getX(), point.getY());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Circle circle = (Circle) o;
        return Objects.equals(center, circle.center) &&
                Objects.equals(radius, circle.radius);
    }

    @Override
    public int hashCode() {
        return Objects.hash(center, radius);
    }
}
