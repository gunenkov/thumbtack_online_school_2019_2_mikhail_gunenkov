package net.thumbtack.school.figures.v1;

public class CircleFactory {
    private static int countOfCircles;

    public static Circle createCircle(Point center, int radius) {
        countOfCircles++;
        return new Circle(center, radius);
    }

    public static int getCircleCount() {
        return countOfCircles;
    }

    public static void reset() {
        countOfCircles = 0;
    }
}


