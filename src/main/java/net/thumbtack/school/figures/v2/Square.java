package net.thumbtack.school.figures.v2;

import java.util.Objects;

public class Square extends Figure {
    private Point topLeft;
    private Point bottomRight;

    public Square(Point leftTop, int size) {
        topLeft = leftTop;
        bottomRight = new Point(leftTop.getX() + size, leftTop.getY() + size);
    }

    public Square(int xLeft, int yTop, int size) {
        this(new Point(xLeft, yTop), size);
    }

    public Square(int size) {
        //FIXED
        // REVU вызовите другой конструктор - this(...)
        this(0, -size, size);
    }

    public Square() {
        this(1);
    }

    public Point getTopLeft() {
        return topLeft;
    }

    public Point getBottomRight() {
        return bottomRight;
    }

    public void setTopLeft(Point topLeft) {
        int newLength = getLength();

        this.topLeft = topLeft;
        bottomRight.setX(topLeft.getX() + newLength);
        bottomRight.setY(topLeft.getY() + newLength);
    }

    public void setBottomRight(Point bottomRight) {
        int newLength = getLength();

        this.bottomRight = bottomRight;
        topLeft.setX(bottomRight.getX() - newLength);
        topLeft.setY(bottomRight.getY() - newLength);
    }

    public int getLength() {
        return bottomRight.getX() - topLeft.getX();
    }

    @Override
    public void moveTo(int x, int y) {
        int size = getLength();

        topLeft = new Point(x, y);
        bottomRight = new Point(x + size, y + size);
    }

    @Override
    public void moveRel(int dx, int dy) {
        topLeft.setX(topLeft.getX() + dx);
        topLeft.setY(topLeft.getY() + dy);
        bottomRight.setX(bottomRight.getX() + dx);
        bottomRight.setY(bottomRight.getY() + dy);
    }

    @Override
    public void resize(double ratio) {
        int newSize = (int) (getLength() * ratio);

        bottomRight.setX(topLeft.getX() + newSize);
        bottomRight.setY(topLeft.getY() + newSize);
    }

    @Override
    public double getArea() {
        return getLength() * getLength();
    }

    @Override
    public double getPerimeter() {
        return 4 * getLength();
    }

    @Override
    public boolean isInside(int x, int y) {
        return x >= topLeft.getX() && x <= bottomRight.getX()
                && y >= topLeft.getY() && y <= bottomRight.getY();
    }

    public boolean isIntersects(Square square) {
        return this.isInside(square.getTopLeft()) ||
                this.isInside(square.getBottomRight()) ||
                square.isInside(topLeft) || square.isInside(bottomRight);
    }

    public boolean isInside(Square square) {
        return (this.isInside(square.getTopLeft()) &&
                this.isInside(square.getBottomRight())) &&
                square.getTopLeft() != getTopLeft() &&
                square.getBottomRight() != getBottomRight();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Square square = (Square) o;
        return Objects.equals(topLeft, square.topLeft) &&
                Objects.equals(bottomRight, square.bottomRight);
    }

    @Override
    public int hashCode() {
        return Objects.hash(topLeft, bottomRight);
    }
}
