package net.thumbtack.school.figures.v2;

import net.thumbtack.school.iface.v2.Colored;

import java.util.Objects;

public class ColoredCircle extends Circle implements Colored {
    private int color = 1;

    public ColoredCircle(Point center, int radius, int color) {
        // FIXED
        // REVU вызовите другой конструктор - this(...)
        this(center.getX(), center.getY(), radius, color);
    }

    public ColoredCircle(int xCenter, int yCenter, int radius, int color) {
        super(xCenter, yCenter, radius);
        this.color = color;
    }

    public ColoredCircle(int radius, int color) {
        //FIXED
        // REVU вызовите другой конструктор - this(...)
        this(new Point(), radius, color);
    }

    public ColoredCircle(int color) {
        //FIXED
        // REVU вызовите другой конструктор - this(...)
        this(1, color);
    }

    public ColoredCircle() {
        super();
    }

    @Override
    public int getColor() {
        return color;
    }

    @Override
    public void setColor(int color) {
        this.color = color;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        ColoredCircle that = (ColoredCircle) o;
        return color == that.color;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), color);
    }
}
