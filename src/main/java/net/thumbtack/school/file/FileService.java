package net.thumbtack.school.file;

import com.google.gson.Gson;
import net.thumbtack.school.colors.v3.ColorException;
import net.thumbtack.school.figures.v3.ColoredRectangle;
import net.thumbtack.school.figures.v3.Rectangle;
import net.thumbtack.school.ttschool.Trainee;
import net.thumbtack.school.ttschool.TrainingException;

import java.io.*;

public class FileService {
    public static void writeByteArrayToBinaryFile(String fileName, byte[] array) throws IOException {
        writeByteArrayToBinaryFile(new File(fileName), array);
    }

    public static void writeByteArrayToBinaryFile(File file, byte[] array) throws IOException {
        try (FileOutputStream fos = new FileOutputStream(file)) {
            fos.write(array);
        }
    }

    public static byte[] readByteArrayFromBinaryFile(String fileName) throws IOException {
        return readByteArrayFromBinaryFile(new File(fileName));
    }

    public static byte[] readByteArrayFromBinaryFile(File file) throws IOException {
        try (FileInputStream fis = new FileInputStream(file)) {
            byte[] input = new byte[(int) file.length()];
            fis.read(input);
            return input;
        }
    }

    public static byte[] writeAndReadByteArrayUsingByteStream(byte[] array) throws IOException {
        try (ByteArrayOutputStream bos = new ByteArrayOutputStream(array.length)) {
            byte[] input = new byte[array.length];
            bos.write(array);
            try (ByteArrayInputStream bis = new ByteArrayInputStream(bos.toByteArray())) {
                bis.read(input);
                byte[] result = new byte[array.length / 2];
                for (int i = 0, j = 0; i < result.length; i++, j += 2) {
                    result[i] = input[j];
                }
                return result;
            }
        }
    }

    public static void writeByteArrayToBinaryFileBuffered(String fileName, byte[] array) throws IOException {
        writeByteArrayToBinaryFile(new File(fileName), array);
    }

    public static void writeByteArrayToBinaryFileBuffered(File file, byte[] array) throws IOException {
        try (BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(file))) {
            bos.write(array);
        }
    }

    public static byte[] readByteArrayFromBinaryFileBuffered(String fileName) throws IOException {
        return readByteArrayFromBinaryFileBuffered(new File(fileName));
    }

    public static byte[] readByteArrayFromBinaryFileBuffered(File file) throws IOException {
        try (BufferedInputStream bis = new BufferedInputStream(new FileInputStream(file))) {
            byte[] input = new byte[(int) file.length()];
            bis.read(input);
            return input;
        }
    }

    public static void writeRectangleToBinaryFile(File file, Rectangle rect) throws IOException {
        writeRectangleArrayToBinaryFile(file, new Rectangle[]{rect});
    }

    public static Rectangle readRectangleFromBinaryFile(File file) throws IOException {
        return readRectangleArrayFromBinaryFileReverse(file)[0];
    }

    public static void writeColoredRectangleToBinaryFile(File file, ColoredRectangle rect) throws IOException {
        writeRectangleToBinaryFile(file, rect);
        try (RandomAccessFile raf = new RandomAccessFile(file, "rw")) {
            raf.seek(16);
            raf.writeUTF(rect.getColor().toString());
        }
    }

    public static ColoredRectangle readColoredRectangleFromBinaryFile(File file) throws IOException, ColorException {
        try (RandomAccessFile raf = new RandomAccessFile(file, "r")) {
            Rectangle rect = new Rectangle(raf.readInt(), raf.readInt(), raf.readInt(), raf.readInt());
            String color = raf.readUTF();
            return new ColoredRectangle(rect.getTopLeft(), rect.getBottomRight(), color);
        }
    }

    public static void writeRectangleArrayToBinaryFile(File file, Rectangle[] rects) throws IOException {
        try (DataOutputStream dos = new DataOutputStream(new FileOutputStream(file))) {
            for (Rectangle rectangle : rects) {
                dos.writeInt(rectangle.getTopLeft().getX());
                dos.writeInt(rectangle.getTopLeft().getY());
                dos.writeInt(rectangle.getBottomRight().getX());
                dos.writeInt(rectangle.getBottomRight().getY());
            }
        }
    }

    public static Rectangle[] readRectangleArrayFromBinaryFileReverse(File file) throws IOException {
        try (RandomAccessFile raf = new RandomAccessFile(file, "r")) {
            Rectangle[] rectangles = new Rectangle[(int) file.length() / 16];
            for (int i = 0; i < rectangles.length; i++) {
                raf.seek(file.length() - 16 * (i + 1));
                rectangles[i] = new Rectangle(raf.readInt(), raf.readInt(), raf.readInt(), raf.readInt());
            }
            return rectangles;
        }
    }

    public static void writeRectangleToTextFileOneLine(File file, Rectangle rect) throws IOException {
        try (FileWriter fw = new FileWriter(file)) {
            fw.write(String.format("%d %d %d %d", rect.getTopLeft().getX(), rect.getTopLeft().getY(), rect.getBottomRight().getX(), rect.getBottomRight().getY()));
        }
    }

    public static Rectangle readRectangleFromTextFileOneLine(File file) throws IOException {
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            String[] fields = br.readLine().split(" ");
            return new Rectangle(Integer.parseInt(fields[0]), Integer.parseInt(fields[1]), Integer.parseInt(fields[2]), Integer.parseInt(fields[3]));
        }
    }

    public static void writeRectangleToTextFileFourLines(File file, Rectangle rect) throws IOException {
        try (FileWriter fw = new FileWriter(file)) {
            fw.write(String.format("%d\n%d\n%d\n%d", rect.getTopLeft().getX(), rect.getTopLeft().getY(), rect.getBottomRight().getX(), rect.getBottomRight().getY()));
        }
    }

    public static Rectangle readRectangleFromTextFileFourLines(File file) throws IOException {
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            String[] fields = br.lines().toArray(String[]::new);
            return new Rectangle(Integer.parseInt(fields[0]), Integer.parseInt(fields[1]), Integer.parseInt(fields[2]), Integer.parseInt(fields[3]));
        }
    }

    public static void writeTraineeToTextFileOneLine(File file, Trainee trainee) throws IOException {
        try (OutputStreamWriter fw = new OutputStreamWriter(new FileOutputStream(file), "UTF-8")) {
            fw.write(String.format("%s %d", trainee.getFullName(), trainee.getRating()));
        }
    }

    public static Trainee readTraineeFromTextFileOneLine(File file) throws IOException, TrainingException {
        try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF-8"))) {
            String[] fields = br.readLine().split(" ");
            return new Trainee(fields[0], fields[1], Integer.parseInt(fields[2]));
        }
    }

    public static void writeTraineeToTextFileThreeLines(File file, Trainee trainee) throws IOException {
        try (OutputStreamWriter fw = new OutputStreamWriter(new FileOutputStream(file), "UTF-8")) {
            fw.write(String.format("%s\n%s\n%d", trainee.getFirstName(), trainee.getLastName(), trainee.getRating()));
        }
    }

    public static Trainee readTraineeFromTextFileThreeLines(File file) throws IOException, TrainingException {
        try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF-8"))) {
            String[] fields = br.lines().toArray(String[]::new);
            return new Trainee(fields[0], fields[1], Integer.parseInt(fields[2]));
        }
    }

    public static void serializeTraineeToBinaryFile(File file, Trainee trainee) throws IOException {
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file))) {
            oos.writeObject(trainee);
        }
    }

    public static Trainee deserializeTraineeFromBinaryFile(File file) throws IOException, ClassNotFoundException {
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file))) {
            return (Trainee) ois.readObject();
        }
    }

    public static String serializeTraineeToJsonString(Trainee trainee) {
        return new Gson().toJson(trainee);
    }

    public static Trainee deserializeTraineeFromJsonString(String json) {
        return new Gson().fromJson(json, Trainee.class);
    }

    public static void serializeTraineeToJsonFile(File file, Trainee trainee) throws IOException {
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(file))) {
            new Gson().toJson(trainee, bw);
        }
    }

    public static Trainee deserializeTraineeFromJsonFile(File file) throws IOException {
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            return new Gson().fromJson(br, Trainee.class);
        }
    }
}
