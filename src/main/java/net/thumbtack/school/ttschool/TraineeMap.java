package net.thumbtack.school.ttschool;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class TraineeMap {
    Map<Trainee, String> traineesToInstitutes;

    public TraineeMap() {
        this.traineesToInstitutes = new HashMap<>();
    }

    public void addTraineeInfo(Trainee trainee, String institute) throws TrainingException {
        if (traineesToInstitutes.containsKey(trainee)) throw new TrainingException(TrainingErrorCode.DUPLICATE_TRAINEE);
        traineesToInstitutes.put(trainee, institute);
    }

    public void replaceTraineeInfo(Trainee trainee, String institute) throws TrainingException {
        if (!traineesToInstitutes.containsKey(trainee))
            throw new TrainingException(TrainingErrorCode.TRAINEE_NOT_FOUND);
        traineesToInstitutes.replace(trainee, institute);
    }

    public void removeTraineeInfo(Trainee trainee) throws TrainingException {
        if (!traineesToInstitutes.containsKey(trainee))
            throw new TrainingException(TrainingErrorCode.TRAINEE_NOT_FOUND);
        traineesToInstitutes.remove(trainee);
    }

    public int getTraineesCount() {
        return traineesToInstitutes.size();
    }

    public String getInstituteByTrainee(Trainee trainee) throws TrainingException {
        if (!traineesToInstitutes.containsKey(trainee))
            throw new TrainingException(TrainingErrorCode.TRAINEE_NOT_FOUND);
        return traineesToInstitutes.get(trainee);
    }

    public Set<Trainee> getAllTrainees() {
        return traineesToInstitutes.keySet();
    }

    public Set<String> getAllInstitutes() {
        return new HashSet<>(traineesToInstitutes.values());
    }

    public boolean isAnyFromInstitute(String institute) {
        return traineesToInstitutes.containsValue(institute);
    }
}
