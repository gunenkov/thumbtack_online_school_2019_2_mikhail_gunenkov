package net.thumbtack.school.matrix;

import java.util.*;
import java.util.stream.Collectors;

public class MatrixNonSimilarRows {
    private Map<Set<Integer>, int[]> setsOfNumbersToRows;

    public MatrixNonSimilarRows(int[][] matrix) {
        setsOfNumbersToRows = new HashMap<>();

        for (int[] row : matrix) {
            Set<Integer> uniqueNumbersInRow = Arrays.stream(row).boxed().collect(Collectors.toSet());
            if (setsOfNumbersToRows.containsKey(uniqueNumbersInRow)) continue;
            setsOfNumbersToRows.put(uniqueNumbersInRow, row);
        }
    }

    public Set<int[]> getNonSimilarRows() {
        return new HashSet<>(setsOfNumbersToRows.values());
    }
}

