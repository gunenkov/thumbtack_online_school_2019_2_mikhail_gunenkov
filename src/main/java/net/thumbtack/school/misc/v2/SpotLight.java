package net.thumbtack.school.misc.v2;

import net.thumbtack.school.figures.v2.Point;
import net.thumbtack.school.iface.v2.Colored;
import net.thumbtack.school.iface.v2.Movable;

import java.util.Objects;

public class SpotLight implements Movable, Colored {
    private Point position;
    private int color;

    public SpotLight(Point position, int color) {
        this.position = position;
        this.color = color;
    }

    public SpotLight(int x, int y, int color) {
        this(new Point(x, y), color);
    }

    public SpotLight(int color) {
        this(0, 0, color);
    }

    public SpotLight() {
        this(1);
    }

    public Point getPosition() {
        return position;
    }

    public void setPosition(Point position) {
        this.position = position;
    }

    @Override
    public void setColor(int color) {
        this.color = color;
    }

    @Override
    public int getColor() {
        return color;
    }

    @Override
    public void moveTo(int x, int y) {
        position.setX(x);
        position.setY(y);
    }

    @Override
    public void moveRel(int dx, int dy) {
        position.setX(position.getX() + dx);
        position.setY(position.getY() + dy);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SpotLight spotLight = (SpotLight) o;
        return color == spotLight.color &&
                Objects.equals(position, spotLight.position);
    }

    @Override
    public int hashCode() {
        return Objects.hash(position, color);
    }
}
