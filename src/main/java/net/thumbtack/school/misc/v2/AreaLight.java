package net.thumbtack.school.misc.v2;

import net.thumbtack.school.figures.v2.Point;
import net.thumbtack.school.iface.v2.HasArea;

import java.util.Objects;

public class AreaLight extends SpotLight implements HasArea {
    private double area;

    public AreaLight(Point position, int color, double area) {
        super(position, color);
        this.area = area;
    }

    public AreaLight(int x, int y, int color, double area) {
        this(new Point(x, y), color, area);
    }

    public AreaLight(int color, double area) {
        this(0, 0, color, area);
    }

    public AreaLight(double area) {
        this(1, area);
    }

    public AreaLight() {
        this(0);
    }

    public void setArea(double area) {
        this.area = area;
    }

    @Override
    public double getArea() {
        return area;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        AreaLight areaLight = (AreaLight) o;
        return Double.compare(areaLight.area, area) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), area);
    }
}
