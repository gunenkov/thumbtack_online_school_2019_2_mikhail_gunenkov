package net.thumbtack.school.misc.v3;

import net.thumbtack.school.colors.v3.Color;
import net.thumbtack.school.colors.v3.ColorException;
import net.thumbtack.school.figures.v3.Point;
import net.thumbtack.school.iface.v3.Colored;
import net.thumbtack.school.iface.v3.Movable;

import java.util.Objects;

public class SpotLight implements Movable, Colored {
    private Point position;
    private Color color;

    public SpotLight(Point position, Color color) {
        this.position = position;
        this.color = color;
    }

    public SpotLight(Point position, String colorString) throws ColorException {
        this(position, Color.colorFromString(colorString));
    }

    public SpotLight(int x, int y, Color color) {
        this(new Point(x, y), color);
    }

    public SpotLight(int x, int y, String colorString) throws ColorException {
        this(x, y, Color.colorFromString(colorString));
    }

    public SpotLight(Color color) {
        this(0, 0, color);
    }

    public SpotLight(String colorString) throws ColorException {
        this(Color.colorFromString(colorString));
    }

    public SpotLight() {
        this(Color.RED);
    }

    public Point getPosition() {
        return position;
    }

    public void setPosition(Point position) {
        this.position = position;
    }

    @Override
    public void setColor(Color color) {
        this.color = color;
    }

    @Override
    public Color getColor() {
        return color;
    }

    @Override
    public void moveTo(int x, int y) {
        position.setX(x);
        position.setY(y);
    }

    @Override
    public void moveRel(int dx, int dy) {
        position.setX(position.getX() + dx);
        position.setY(position.getY() + dy);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SpotLight spotLight = (SpotLight) o;
        return color == spotLight.color &&
                Objects.equals(position, spotLight.position);
    }

    @Override
    public int hashCode() {
        return Objects.hash(position, color);
    }
}
