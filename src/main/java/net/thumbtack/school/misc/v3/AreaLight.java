package net.thumbtack.school.misc.v3;

import net.thumbtack.school.colors.v3.Color;
import net.thumbtack.school.colors.v3.ColorException;
import net.thumbtack.school.figures.v3.Point;
import net.thumbtack.school.iface.v3.HasArea;

import java.util.Objects;

public class AreaLight extends SpotLight implements HasArea {
    private double area;

    public AreaLight(Point position, Color color, double area) {
        super(position, color);
        this.area = area;
    }

    public AreaLight(Point position, String colorString, double area) throws ColorException {
        this(position, Color.colorFromString(colorString), area);
    }

    public AreaLight(int x, int y, Color color, double area) {
        this(new Point(x, y), color, area);
    }

    public AreaLight(int x, int y, String colorString, double area) throws ColorException {
        this(x, y, Color.colorFromString(colorString), area);
    }

    public AreaLight(Color color, double area) {
        this(0, 0, color, area);
    }

    public AreaLight(String colorString, double area) throws ColorException {
        this(Color.colorFromString(colorString), area);
    }

    public AreaLight(double area) {
        this(Color.RED, area);
    }

    public AreaLight() {
        this(0);
    }

    public void setArea(double area) {
        this.area = area;
    }

    @Override
    public double getArea() {
        return area;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        AreaLight areaLight = (AreaLight) o;
        return Double.compare(areaLight.area, area) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), area);
    }
}
